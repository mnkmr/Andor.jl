module Andor

include("finddll.jl")
include("sdk.jl"); import .Dll
include("constants.jl")
include("errors.jl")

const DLLPATH, DLLDIR = finddll()

# Nothing to be able to do if no dll found
if !isfile(DLLPATH)
    error_dll_not_found()
end


function testshot(trigger=Internal)
    Dll.open(DLLDIR, DLLPATH) do dll
        name = Dll.GetHeadModel(dll)
        println("camera: $(name)")

        y, x = Dll.GetDetector(dll)
        println("size: $(y)x$(x)")

        mintemp, maxtemp = Dll.GetTemperatureRange(dll)
        println("temperature: $(mintemp) ~ $(maxtemp)")

        temperature = (mintemp + maxtemp)÷2
        Dll.SetTemperature(dll, temperature)
        println("Set the ccd temperature to $(temperature) degree")

        Dll.CoolerON()
        println("Start cooling")

        while true
            _, rc = Dll.GetTemperature(dll)
            if rc == DRV_TEMP_STABILIZED
                println("The ccd temperature has been stabilized")
                break
            end
        end

        Dll.SetAcquisitionMode(dll, SingleScan)
        println("acquisition mode: Single Scan")

        Dll.SetReadMode(dll, Image)
        println("readmode: Image")

        Dll.SetExposureTime(dll, 1e-3)
        println("exposure: 1 ms")

        Dll.SetTriggerMode(dll, Internal)
        trig = trigger == Internal ? "Internal" : "External"
        println("trigger: $(trie)")

        Dll.StartAcquisition(dll)
        while GetStatus(dll) == DRV_ACQUIRING
            sleep(5e-3)
        end
        buf = Dll.GetAcquiredData(dll, y*x)
        img = reshape(buf, y, x)

        Dll.CoolerOFF()
        println("Stop cooling")

        while true
            temperature, _ = Dll.GetTemperature(dll)
            if temperature > -20
                println("The ccd temperature has been warm up enough")
                break
            end
        end

        img
    end
end


end # module
