# Return codes
const DRV_ERROR_CODES         = UInt32(20001)
const DRV_SUCCESS             = UInt32(20002)
const DRV_VXNOTINSTALLED      = UInt32(20003)
const DRV_ERROR_FILELOAD      = UInt32(20006)
const DRV_ERROR_VXD_INIT      = UInt32(20007)
const DRV_ERROR_PAGELOCK      = UInt32(20010)
const DRV_ERROR_PAGE_UNLOCK   = UInt32(20011)
const DRV_ERROR_ACK           = UInt32(20013)
const DRV_NO_NEW_DATA         = UInt32(20024)
const DRV_SPOOLERROR          = UInt32(20026)
const DRV_TEMP_OFF            = UInt32(20034)
const DRV_TEMP_NOT_STABILIZED = UInt32(20035)
const DRV_TEMP_STABILIZED     = UInt32(20036)
const DRV_TEMP_NOT_REACHED    = UInt32(20037)
const DRV_TEMP_OUT_RANGE      = UInt32(20038)
const DRV_TEMP_NOT_SUPPORTED  = UInt32(20039)
const DRV_TEMP_DRIFT          = UInt32(20040)
const DRV_COF_NOTLOADED       = UInt32(20050)
const DRV_FLEXERROR           = UInt32(20053)
const DRV_P1INVALID           = UInt32(20066)
const DRV_P2INVALID           = UInt32(20067)
const DRV_P3INVALID           = UInt32(20068)
const DRV_P4INVALID           = UInt32(20069)
const DRV_INIERROR            = UInt32(20070)
const DRV_COERROR             = UInt32(20071)
const DRV_ACQUIRING           = UInt32(20072)
const DRV_IDLE                = UInt32(20073)
const DRV_TEMPCYCLE           = UInt32(20074)
const DRV_NOT_INITIALIZED     = UInt32(20075)
const DRV_P5INVALID           = UInt32(20076)
const DRV_P6INVALID           = UInt32(20077)
const P7_INVALID              = UInt32(20083)
const DRV_USBERROR            = UInt32(20089)
const DRV_BINNING_ERROR       = UInt32(20099)
const DRV_NOCAMERA            = UInt32(20990)
const DRV_NOT_SUPPORTED       = UInt32(20991)
const DRV_NOT_AVAILABLE       = UInt32(20992)


# Acquisition mode
@enum AcquisitionMode::Cint begin
    SingleScan
    Accumulate
    Kinetics
    FastKinetics
    RunTillAbort
end


# Read mode
@enum ReadMode::Cint begin
    FullVerticalBinning = 0
    MultiTrack = 1
    RandomTrack = 2
    SingleTrack = 3
    Image = 4
end


# Trigger mode
@enum TriggerMode::Cint begin
    Internal = 0
    External = 1
    ExternalStart = 6
    ExternalExposure = 7
    ExternalFVBEM = 9
    SoftwareTrigger = 10
end


# Miscellaneous
const MAX_PATH = 260
