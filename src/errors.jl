# Errors

# atmcdxxd dll does not found
function error_dll_notfound()
    if Sys.WORD_SIZE == 64
        dllname = "atmcd64d.dll"
    else
        dllname = "atmcd32d.dll"
    end
    error("Andor: $(dllname) not found")
end

function error_in_calling_dll_function(rc)
    error("SIFReader: error in calling a function of ATSIFIO library: $(rc)")
end
