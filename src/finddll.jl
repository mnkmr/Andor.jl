# Search atmcd32d.dll (atmcd64d.dll)
function finddll()
    dllpath = ""
    dlldir = ""
    AndorSOLIS = "Andor SOLIS"
    if Sys.WORD_SIZE == 64
        ProgramFiles = ENV["PROGRAMFILES"]
        dllname = "atmcd64d.dll"
    else
        ProgramFiles = ENV["PROGRAMFILES(x86)"]
        dllname = "atmcd32d.dll"
    end
    path = joinpath(ProgramFiles, AndorSOLIS, dllname)
    if isfile(path)
        dllpath = path
        dlldir = joinpath(ProgramFiles, AndorSOLIS)
    end
    dllpath, dlldir
end

