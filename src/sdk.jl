module Dll

using Libdl

include("constants.jl")
include("errors.jl")

struct AndorDll
    ptr::Ptr{Nothing}
    dir::String
    path::String
end


function open(dlldir::String, dllpath::String)
    dllptr = dlopen(dllpath)
    AndorDll(dllptr, dlldir, dllpath)
end

function open(f::Function, dlldir::String, dllpath::String)
    dll = open(dlldir, dllpath)
    try
        Initialize(dll)
        f(dll)
    finally
        ShutDown(dll)
        close(dll)
    end
end


function close(dll::AndorDll)
    dlclose(dll.ptr)
end


# An wrapper of Libdl.dlsym, a short hand for AndorDll type
Libdl.dlsym(dll::AndorDll, sym::Symbol) = dlsym(dll.ptr, sym)


# Convert Cstring to String safely
function string(str)
    str[end] = 0
    unsafe_string(pointer(str))
end


function Initialize(dll::AndorDll)
    F = dlsym(dll, :Initialize)
    rc = ccall(F, UInt32, (Cstring,), dll.dir)
    returncode_check(rc)
end


function GetHeadModel(dll::AndorDll)
    name = Array{Cchar,1}(undef, MAX_PATH)
    F = dlsym(dll, :GetHeadModel)
    rc = ccall(F, UInt32, (Ptr{Cchar},), name)
    returncode_check(rc)
    string(name)
end


function GetDetector(dll::AndorDll)
    xpixels = zeros(Cint, 1)
    ypixels = zeros(Cint, 1)
    F = dlsym(dll, :GetDetector)
    rc = ccall(F, UInt32, (Ptr{Cint}, Ptr{Cint}), xpixels, ypixels)
    returncode_check(rc)
    Int(ypixels[1]), Int(xpixels[1])
end


function GetTemperatureRange(dll::AndorDll)
    mintemp = zeros(Cint, 1)
    maxtemp = zeros(Cint, 1)
    F = dlsym(dll, :GetTempertureRange)
    rc = ccall(F, UInt32, (Ptr{Cint}, Ptr{Cint}), mintemp, maxtemp)
    returncode_check(rc)
    Int(mintemp[1]), Int(maxtemp[1])
end


function GetTemperature(dll::AndorDll)
    temperature = zeros(Cint, 1)
    F = dlsym(dll, :GetTemperature)
    rc = ccall(F, UInt32, (Ptr{Cint},), temperature)
    Int(temperature[1]), rc
end


function SetTemperature(dll::AndorDll, temperature::Integer)
    F = dlsym(dll, :SetTemperature)
    rc = ccall(F, UInt32, (Cint,), Cint(temperature))
    returncode_check(rc)
end


function CoolerON()
    F = dlsym(dll, :CoolerON)
    rc = ccall(F, UInt32, ())
    returncode_check(rc)
end


function CoolerOFF()
    F = dlsym(dll, :CoolerOFF)
    rc = ccall(F, UInt32, ())
    returncode_check(rc)
end


function SetAcquisitionMode(dll::AndorDll, mode::AcquisitionMode)
    F = dlsym(dll, :SetAcquisitionMode)
    rc = ccall(F, UInt32, (Cint,), Cint(mode))
    returncode_check(rc)
end


function SetReadMode(dll::AndorDll, mode::ReadMode)
    F = dlsym(dll, :SetReadMode)
    rc = ccall(F, UInt32, (Cint,), Cint(mode))
    returncode_check(rc)
end


function SetExposureTime(dll::AndorDll, time::Real)
    F = dlsym(dll, :SetExposureTime)
    rc = ccall(F, UInt32, (Cint,), Cfloat(time))
    returncode_check(rc)
end


function SetTriggerMode(dll::AndorDll, mode::TriggerMode)
    F = dlsym(dll, :SetTriggerMode)
    rc = ccall(F, UInt32, (Cint,), Cint(mode))
    returncode_check(rc)
end


function StartAcquisition(dll::AndorDll)
    F = dlsym(dll, :StartAcquisition)
    rc = ccall(F, UInt32, ())
    returncode_check(rc)
end


function GetStatus(dll::AndorDll)
    status = zeros(Cint, 1)
    F = dlsym(dll, :GetStatus)
    rc = ccall(F, UInt32, (Ptr{Cint},), status)
    returncode_check(rc)
    Int(status[1])
end


function GetAcquiredData(dll::AndorDll, size::Integer)
    array = zeros(Cint, size)
    F = dlsym(dll, :GetAcquiredData)
    rc = ccall(F, UInt32, (Ptr{Cint},Cint), array, Cuint(size))
    returncode_check(rc)
    array
end


function ShutDown(dll::AndorDll)
    F = dlsym(dll, :ShutDown)
    rc = ccall(F, UInt32, ())
    returncode_check(rc)
end


function returncode_check(rc; allow=[])
    if rc == DRV_SUCCESS
        return nothing
    end
    if rc in allow
        return nothing
    end
    error_in_calling_dll_function(rc)
end

end
